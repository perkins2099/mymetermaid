class NotificationWorker
  include Sidekiq::Worker
  sidekiq_options queue: "notification"
  sidekiq_options :retry => 10

  def perform(user_id, parking_event_id)
    user = User.find(user_id)
    parking_event = ParkingEvent.find(parking_event_id)
    raise "No User phone_number" if user.phone_number.blank?
    time_left = ((Time.now - parking_event.created_at) / 60).round(1)
    message = 'Your meter is going to expire in ' + time_left.to_s+' minutes!'
    #begin
      @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'],ENV['TWILIO_AUTH_TOKEN']
      @client.account.sms.messages.create(
        :from => ENV['TWILIO_PHONE_NUMBER'],
        :to => user.phone_number,
        :body => message)
    Notification.create(message: message, user_id: user.id, reason: "Meter Expiration Warning", status: "Success")
    #rescue
    #end
  end

  def retries_exhausted(*args)
    #error = "Had to fire this guy, he wouldn't work"
    #ExceptionNotifier::Notifier.background_exception_notification(Exception.new(error))
    Notification.create(user_id: user.id, message:parking_event_id.to_s ,reason: "Meter Expiration Warning", status: "Failure")
  end
end