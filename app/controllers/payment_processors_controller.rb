class PaymentProcessorsController < ActionController::Base
  before_filter :authenticate_user!
  layout "application"

  def index
    @payment_processors = PaymentProcessor.where(user_id: current_user.id, status: 'Active')
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @payment_processors }
      format.json { render :json => @payment_processors }
    end
  end

  def show
    @payment_processor = PaymentProcessor.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @payment_processor }
      format.json { render :json => @payment_processor }
    end
  end

  def destroy
    @pp = current_user.payment_processors.find(params[:id])
    @pp.status = 'Inactive'
    @pp.save

  end

  def new

  end

  def create
    puts params.to_yaml
    
  end

end
