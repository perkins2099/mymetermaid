class DashController < ActionController::Base
  before_filter :authenticate_user!
  layout "application"

  def index
    if !current_user
      redirect_to root
    elsif current_user.peo?
      render '/dash/peo_dashboard'
    elsif current_user.customer?
      render '/dash/customer_dashboard'
    end
  end

end
