class ParkingSpacesController < ActionController::Base
  before_filter :authenticate_user!
  layout "application"

  def index
    @parking_spaces = ParkingSpace.all
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @parking_spaces }
      format.json { render :json => @parking_spaces }
    end
  end

  def show
   @parking_space = ParkingSpace.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @parking_space }
      format.json { render :json => @parking_space }
    end
  end

end
