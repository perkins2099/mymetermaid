class ParkingEventsController < ActionController::Base
  before_filter :authenticate_user!
  layout "application"

  def index
  end

  def new
    @parking_event = ParkingEvent.new
    @default_payment_method = current_user.primary_payment_method
  end

  def show
  end

  def destroy

  end

  def create
    save_card = if params[:save_card] == "1"
        true
      else
        false
      end
    default_payment_method = current_user.primary_payment_method

    @parking_event = ParkingEvent.new()
    @parking_event.customer_id = current_user.customer_id
    #binding.pry
    @parking_event.duration_in_minutes = params[:parking][:duration]#params[:duration]
    @parking_space = ParkingSpace.find_by_identifier(params[:identifier])
    @parking_event.parking_space_id = @parking_space.id if @parking_space
    
    amount = params[:amount_in_cents]
    @parking_event.cost_in_cents = amount

    charge_status = nil
    payment_processor = nil
    if default_payment_method
      charge_status = default_payment_method.charge(amount)
      payment_processor = default_payment_method
    elsif params[:stripe_card_token]
      if !save_card
        payment_processor = PaymentProcessor::StripeMmm.create(name: "Stripe", user_id: current_user.id, token: params[:stripe_card_token])
        payment_processor.set_customer
        charge_status = payment_processor.charge(amount)
        payment_processor.status = "Inactive"
        payment_processor.authorization[:last4] = charge_status.card.last4
        payment_processor.save
        #charge_status = PaymentProcessor::StripeMmm.one_time_charge(params[:stripe_card_token], 200)
      else
        payment_processor = PaymentProcessor::StripeMmm.create(name: "Stripe", user_id: current_user.id, token: params[:stripe_card_token])
        payment_processor.set_customer
        charge_status = payment_processor.charge(amount)
        if charge_status.paid
          payment_processor.status = 'Active'
          payment_processor.primary = true
          payment_processor.authorization[:last4] = charge_status.card.last4
          payment_processor.save
        end
      end
    end

    if charge_status.paid and payment_processor
      begin
        @parking_event.save
        my_meter_maid_debit = PaymentProcessor::MyMeterMaid.find_by_name('My Meter Maid Dedit')
        my_meter_maid_credit = PaymentProcessor::MyMeterMaid.find_by_name('My Meter Maid Credit')
        charge = Charge.create(amount_in_cents: amount, chargeable: payment_processor, user_id:current_user.id)
        charge = Charge.create(amount_in_cents: amount.to_i*-1, chargeable: my_meter_maid_debit, user_id: current_user.id)
        charge = Charge.create(amount_in_cents: amount, chargeable: my_meter_maid_credit, user_id: @parking_space.property_owner.user.id)
      rescue => ex
        puts ex.message
        puts "Oh many the charges didn't work; FIRE FIRE"
      end
      redirect_to confirmation_parking_event_path(@parking_event)
    else
      render :new
    end
  end

end
