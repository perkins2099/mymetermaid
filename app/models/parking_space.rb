class ParkingSpace < ActiveRecord::Base
  attr_accessible :address1, :address2, :city, :identifier, :lat, :long, :price_per_hour_in_cents, :property_owner_id, :state, :zip, :max_duartion_in_minutes

  belongs_to :property_owner
  has_many :events

  validates :identifier, :presence => true
  validates :property_owner_id, :presence => true
  validates :price_per_hour_in_cents, :presence => true

end
