class Peo < ActiveRecord::Base
  attr_accessible :user_id
  
	has_one :user, :as => :profileable

  #validates :phone_number, :presence => true
  validates :user_id, :presence => true

  def customer?
    false
  end
  def peo?
    true
  end
end
