class Notification < ActiveRecord::Base
   attr_accessible :message,:user_id,:reason,:status

  validates :user_id, :presence => true

end
