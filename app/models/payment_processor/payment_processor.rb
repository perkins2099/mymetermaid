class PaymentProcessor < ActiveRecord::Base
  attr_accessible :user_id, :name, :type

  belongs_to :user
  serialize :authorization, Hash 

  validates :name, :presence => true
  validates :user_id, :presence => true
  # validates :type, :uniqueness => { :scope => :user_id,
  #   :message => "Can only create one" }

end
