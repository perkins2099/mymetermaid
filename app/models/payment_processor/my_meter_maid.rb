class PaymentProcessor::MyMeterMaid < PaymentProcessor
#This class is for manual adjustments to account balances
  has_many :charges, :as => :chargeable

end