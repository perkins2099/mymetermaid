class PaymentProcessor::StripeMmm < PaymentProcessor
  has_many :charges, :as => :chargeable
  
  attr_accessor :token
  attr_accessible :token

  def customer_description
    'Card Ending In - ' + authorization[:last4]
  end

  def charge(amount)
    charge = Stripe::Charge.create(
      :customer    => authorization[:stripe_customer_token],
      :amount      => amount,
      :description => 'Parking',
      :currency    => 'usd'
    )
  end
  def set_customer
    customer = Stripe::Customer.create(description: "Parking", card: token)
    self.authorization[:stripe_customer_token] = customer.id
    self.save
  end

  def self.get_customer(token)
    customer = Stripe::Customer.create(description: "Parking", card: token)
  end

  def self.one_time_charge(token, amount)
    customer = get_customer(token)

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => amount,
      :description => 'Parking',
      :currency    => 'usd'
    )
  end

end