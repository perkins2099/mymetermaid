class Customer < ActiveRecord::Base
  attr_accessible :user_id

  has_one :user, :as => :profileable
  has_many :events
  has_many :notifications

  validates :phone_number, :presence => true
  validates :user_id, :presence => true

  def email
    self.user.email
  end
  def customer?
    true
  end
  def peo?
    false
  end

end
