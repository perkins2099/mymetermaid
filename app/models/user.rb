class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :phone_number, :admin, :profileable
  # attr_accessible :title, :body

  belongs_to :profileable, :polymorphic => true
  has_many :charges
  has_many :payment_processors

  after_create :set_up_customer

  validates :phone_number, :presence => true
  validates :phone_number, :length => { :is => 10 }
  validates_format_of :phone_number,
      :with => /[0-9]{3}[0-9]{3}[0-9]{4}/,
      :message => "- Phone numbers must be in xxxxxxxxxx format."


  #Set the appropriate type of user
  def set_up_customer
    if !self.profileable
      customer = Customer.new(user_id: self.id)
      customer.save(validate: false)
      self.profileable = customer
      self.save
    end
  end

  def customer?
    profileable.customer?
  end
  def peo?
    profileable.peo?
  end
  def customer_id
    profileable.id if profileable.customer?
  end

  #Return the customers balance
  def balance
    self.charges.sum(:amount)
  end

  def primary_payment_method
    self.payment_processors.where(primary: true, status: 'Active').first 
  end

end
