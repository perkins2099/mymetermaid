class ParkingEvent < ActiveRecord::Base
  attr_accessible :customer_id, :duration_in_minutes, :event_type, :parking_space_id

  belongs_to :customer
  belongs_to :parking_space
  has_many :charges

  validates :customer_id, :presence => true
  validates :parking_space_id, :presence => true
  validates :duration_in_minutes, :presence => true


  after_create :after_create

  def after_create
    minutes = if self.duration_in_minutes > 30
      self.duration_in_minutes - 15
    else
      self.duration_in_minutes - 5
    end

    NotificationWorker.perform_in(minutes.minutes, self.customer.user.id, self.id)
  end

end
