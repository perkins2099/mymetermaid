class Charge < ActiveRecord::Base
  attr_accessible :amount_in_cents, :user_id, :chargeable

  belongs_to :user
  belongs_to :chargeable, :polymorphic => true
  belongs_to :parking_event

  validates :amount_in_cents, :presence => true
  validates :chargeable_id, :presence => true
  validates :chargeable_type, :presence => true
  validates :user_id, :presence => true

end
