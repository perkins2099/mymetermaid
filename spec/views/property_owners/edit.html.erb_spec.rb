require 'spec_helper'

describe "property_owners/edit" do
  before(:each) do
    @property_owner = assign(:property_owner, stub_model(PropertyOwner,
      :name => "MyText",
      :owner_type => "MyString"
    ))
  end

  it "renders the edit property_owner form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", property_owner_path(@property_owner), "post" do
      assert_select "textarea#property_owner_name[name=?]", "property_owner[name]"
      assert_select "input#property_owner_owner_type[name=?]", "property_owner[owner_type]"
    end
  end
end
