require 'spec_helper'

describe "property_owners/show" do
  before(:each) do
    @property_owner = assign(:property_owner, stub_model(PropertyOwner,
      :name => "MyText",
      :owner_type => "Owner Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/Owner Type/)
  end
end
