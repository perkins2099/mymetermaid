require 'spec_helper'

describe "property_owners/index" do
  before(:each) do
    assign(:property_owners, [
      stub_model(PropertyOwner,
        :name => "MyText",
        :owner_type => "Owner Type"
      ),
      stub_model(PropertyOwner,
        :name => "MyText",
        :owner_type => "Owner Type"
      )
    ])
  end

  it "renders a list of property_owners" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Owner Type".to_s, :count => 2
  end
end
