# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

AdminUser.create!(phone_number:'1231231234',:email => 'admin@mymetermaid.com', :password => 'metersrcool123', :password_confirmation => 'metersrcool123') if AdminUser.count ==0
user = User.find_by_email('admin@mymetermaid.com')
if !user
  user = User.create!(phone_number:'1231231234',:email => 'admin@mymetermaid.com', admin: true, :password => 'metersrcool123', :password_confirmation => 'metersrcool123') 
end

PaymentProcessor::MyMeterMaid.create!(name:"My Meter Maid Credit", user_id: user.id) if  PaymentProcessor::MyMeterMaid.find_by_name("My Meter Maid Credit").blank?
PaymentProcessor::MyMeterMaid.create!(name:"My Meter Maid Dedit", user_id: user.id) if  PaymentProcessor::MyMeterMaid.find_by_name("My Meter Maid Debit").blank?
#PaymentProcessor::StripeMmm.create!(name:"Stripe - One Time", user_id: user.id) if  PaymentProcessor::StripeMmm.find_by_name("Stripe - One Time").blank?



