class CreateNotifications < ActiveRecord::Migration
  def up
    create_table :notifications do |t|
      t.integer :customer_id
      t.string :status
      t.string :reason
      t.text :message
      t.timestamps
    end
    remove_column :charges, :charge_type
    add_index :notifications, :customer_id
    add_column :payment_processors, :primary, :boolean, default: false
    add_index :payment_processors, :primary
  end
  def down
    drop_table :notifications
    add_column :charges, :charge_type, :string
    remove_column :payment_processors, :primary
  end
end
