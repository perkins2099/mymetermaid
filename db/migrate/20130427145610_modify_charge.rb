class ModifyCharge < ActiveRecord::Migration
  def up
    # add_column :charges, :balanceable_id, :integer
    # add_column :charges, :balanceable_type, :string
    # add_index :charges, :balanceable_id
    # add_index :charges, :balanceable_type

    remove_column :charges, :customer_id
    add_column :charges, :user_id, :integer
    add_index :charges, :user_id
    add_column :payment_processors, :user_id, :integer
    add_index :payment_processors, :user_id
    add_column :charges, :notes, :text

  end

  def down
    # remove_column :charges, :balanceable_id 
    # remove_column :charges, :balanceable_type
    add_column :charges, :customer_id, :integer
    remove_column :charges, :user_id
    remove_column :payment_processors, :user_id
    remove_column :charges, :notes
  end
end
