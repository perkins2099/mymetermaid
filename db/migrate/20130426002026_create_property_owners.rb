class CreatePropertyOwners < ActiveRecord::Migration
  def change
    create_table :property_owners do |t|
      t.text :name
      t.string :owner_type

      t.timestamps
    end
  end
end
