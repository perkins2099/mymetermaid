class RemoveCustomerIdFromPaymentProcessor < ActiveRecord::Migration
  def up
    remove_column :payment_processors, :customer_id
  end

  def down
    add_column :payment_processors, :customer_id
  end
end
