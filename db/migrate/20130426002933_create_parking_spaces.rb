class CreateParkingSpaces < ActiveRecord::Migration
  def change
    create_table :parking_spaces do |t|
      t.string :identifier
      t.integer :property_owner_id
      t.string :lat
      t.string :long
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.decimal :price_per_hour , :precision => 8, :scale => 2

      t.timestamps
    end

      add_index :parking_spaces, :property_owner_id
      add_index :parking_spaces, :identifier
  end
end
