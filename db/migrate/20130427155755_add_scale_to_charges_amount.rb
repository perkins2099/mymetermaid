class AddScaleToChargesAmount < ActiveRecord::Migration
  def up
    change_column :charges, :amount, :decimal,:precision => 8, :scale => 2
  end
  def down
    change_column :charges, :amount, :decimal
  end

end
