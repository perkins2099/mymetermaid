class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :customer_id
      t.integer :parking_space_id
      t.string :event_type
      t.integer :duration_in_minutes

      t.timestamps
    end
    add_index :events, :customer_id
    add_index :events, :parking_space_id
  end
end
