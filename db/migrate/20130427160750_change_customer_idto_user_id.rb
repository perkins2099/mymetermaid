class ChangeCustomerIdtoUserId < ActiveRecord::Migration
  def up
    rename_column :notifications, :customer_id, :user_id
  end

  def down
    rename_column :notifications, :user_id, :customer_id
  end
end
