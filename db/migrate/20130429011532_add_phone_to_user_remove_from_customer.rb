class AddPhoneToUserRemoveFromCustomer < ActiveRecord::Migration
  def change
    add_column :users, :phone_number, :string
    remove_column :customers, :phone_number
  end
end
