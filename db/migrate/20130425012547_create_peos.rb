class CreatePeos < ActiveRecord::Migration
  def change
    create_table :peos do |t|
      t.integer :user_id
      t.string :first_name
      t.string :last_name

      t.integer :property_owner_id
      t.timestamps
    end
    add_index :peos, :property_owner_id
  end
end
