class RenameEventsToParkingEvents < ActiveRecord::Migration
  def up
    remove_column :events, :event_type
    rename_table :events, :parking_events
    
  end

  def down
    rename_table :parking_events, :events
    add_column :events, :event_type, :string
  end
end
