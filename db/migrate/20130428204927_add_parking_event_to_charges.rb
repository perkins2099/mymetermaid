class AddParkingEventToCharges < ActiveRecord::Migration
  def change
    add_column :charges, :parking_event_id, :integer
    add_column :parking_events, :cost_in_cents, :integer
    add_index :charges, :parking_event_id
  end
end
