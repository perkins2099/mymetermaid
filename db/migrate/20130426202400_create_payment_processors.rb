class CreatePaymentProcessors < ActiveRecord::Migration
  def change
    create_table :payment_processors do |t|
      t.string :name
      t.integer :customer_id
      t.string :type
      t.text :authorization

      t.timestamps
    end
    add_index :payment_processors, :customer_id
    add_index :payment_processors, :type
  end
end
