class ChangeDecimalToCents < ActiveRecord::Migration
  def up
    remove_column :charges, :amount
    add_column :charges, :amount_in_cents, :integer
    
    remove_column :parking_spaces, :price_per_hour
    add_column :parking_spaces, :price_per_hour_in_cents, :integer

    add_column :payment_processors, :status, :string, default: 'New'
    add_index :payment_processors, :status
  end

  def down
    add_column :charges, :amount, :decimal
    remove_column :charges, :amount_in_cents

    remove_column :parking_spaces, :price_per_hour_in_cents
    add_column :parking_spaces, :price_per_hour, :decimal

    remove_column :payment_processors, :status
  end
end
