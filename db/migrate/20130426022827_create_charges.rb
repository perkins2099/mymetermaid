class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.integer :customer_id
      t.decimal :amount
      t.string :charge_type
      t.belongs_to :chargeable, polymorphic: true

      t.timestamps
    end
    add_index :charges, :customer_id
    add_index :charges, :charge_type
    add_index :charges, :chargeable_type
    add_index :charges, :chargeable_id
  end
end
