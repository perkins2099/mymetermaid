# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130429011532) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "charges", :force => true do |t|
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "chargeable_id"
    t.string   "chargeable_type"
    t.integer  "user_id"
    t.text     "notes"
    t.integer  "amount_in_cents"
    t.integer  "parking_event_id"
  end

  add_index "charges", ["chargeable_id"], :name => "index_charges_on_chargeable_id"
  add_index "charges", ["chargeable_type"], :name => "index_charges_on_chargeable_type"
  add_index "charges", ["parking_event_id"], :name => "index_charges_on_parking_event_id"
  add_index "charges", ["user_id"], :name => "index_charges_on_user_id"

  create_table "customers", :force => true do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "billing_address1"
    t.string   "billing_address2"
    t.string   "billing_city"
    t.string   "billing_state"
    t.string   "billing_zip"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "customers", ["user_id"], :name => "index_customers_on_user_id"

  create_table "notifications", :force => true do |t|
    t.integer  "user_id"
    t.string   "status"
    t.string   "reason"
    t.text     "message"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "notifications", ["user_id"], :name => "index_notifications_on_customer_id"

  create_table "parking_events", :force => true do |t|
    t.integer  "customer_id"
    t.integer  "parking_space_id"
    t.integer  "duration_in_minutes"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "cost_in_cents"
  end

  add_index "parking_events", ["customer_id"], :name => "index_events_on_customer_id"
  add_index "parking_events", ["parking_space_id"], :name => "index_events_on_parking_space_id"

  create_table "parking_spaces", :force => true do |t|
    t.string   "identifier"
    t.integer  "property_owner_id"
    t.string   "lat"
    t.string   "long"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "price_per_hour_in_cents"
    t.integer  "max_duartion_in_minutes"
  end

  add_index "parking_spaces", ["identifier"], :name => "index_parking_spaces_on_identifier"
  add_index "parking_spaces", ["property_owner_id"], :name => "index_parking_spaces_on_property_owner_id"

  create_table "payment_processors", :force => true do |t|
    t.string   "name"
    t.string   "type"
    t.text     "authorization"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.boolean  "primary",       :default => false
    t.integer  "user_id"
    t.string   "status",        :default => "New"
  end

  add_index "payment_processors", ["primary"], :name => "index_payment_processors_on_primary"
  add_index "payment_processors", ["status"], :name => "index_payment_processors_on_status"
  add_index "payment_processors", ["type"], :name => "index_payment_processors_on_type"
  add_index "payment_processors", ["user_id"], :name => "index_payment_processors_on_user_id"

  create_table "peos", :force => true do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "property_owner_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "peos", ["property_owner_id"], :name => "index_peos_on_property_owner_id"

  create_table "property_owners", :force => true do |t|
    t.text     "name"
    t.string   "owner_type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.boolean  "admin",                  :default => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.integer  "profileable_id"
    t.string   "profileable_type"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "phone_number"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
