Mymetermaid::Application.routes.draw do
  require 'sidekiq/web'

  #root :to => 'dash#index'

  root :to => 'pages#show', :id => 'home'
  #get "/pages/*id" => 'pages#show', :as => :page, :format => false

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users

  get "/dash" => "dash#index", as: 'dash'

  resources :payment_processors
  resources :parking_spaces, only: [:index, :show]
  #resources :charges # only: [:new, :index,:show, :destroy, :create]
  resources :parking_events do
    member do
      get '/confirmation' => 'parking_events#confirmation'
    end
  end

  #Sidekiq - Worker Queue
  #constraint = lambda { |request| request.env["warden"].authenticate? and request.env['warden'].user.admin? }
  #constraints constraint do
  authenticate :admin_user do
    mount Sidekiq::Web => '/admin/sidekiq'
  end
  
end
